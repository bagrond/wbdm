
**wbdm** is a web-based configurator and manager for MySQL driven and PHP-powered websites. This also includes automatically created configurable client (Javascript) and server (PHP) constant files, as defined in the database. 

## Getting Started

## Prerequisites

## Installing

## Testing

## Deployment

## Built With
* PHP 5.4.45
* Percona Server 5.5.51
* jQuery XXX
* sAjax XXX

## Contributing

## Versioning

## Authors
* Dave Baker - [bagrond](https://gitlab.com/bagrond)

## License

## Acknowledgments

James MacArthur and the entire Barbed Arrow team, of course.

optionsListArray=new Array();

String.prototype.RemoveClass = function (val) {
    return this.replace(val, "");
}

String.prototype.AddClass = function (val) {
    var str = this.concat(" ",val);
    return str.trim();
}

function globalGetAttribute(id,attr) {
	return document.getElementById(id).getAttribute(attr);
}

function globalSetAttribute(id,attr,val) {
	var g=document.getElementById(id);
	g.setAttribute(attr,val);
}

function globalRemoveAttribute(id,attr) {
	document.getElementById(id).removeAttribute(attr);
}



function confirm_delete() {
	input_box=confirm("Continue with deletion?");
	if (input_box==true) {
		return true; }
	else {
		return false; }
}

function varcharsize() {

var e = document.getElementById("coltype");
var strChk = e.options[e.selectedIndex].value;

	if (strChk=='7') {
		document.getElementById("varcharsize").style.visibility = "visible";
		document.getElementById("optionloader").style.display = "none";
		disableFt(0); }
	else if (strChk=='0' || strChk=='1' || strChk=='2' || strChk=='5' || strChk=='8') {
		document.getElementById("varcharsize").style.visibility = "hidden";
		disableFt(0); }
	else {

		document.getElementById("varcharsize").style.visibility = "hidden";
		document.getElementById("optionloader").style.display = "none";
		disableFt(1);}
}


function optionsEntryField() {

var e = document.getElementById("fieldtype");
var strChk = e.options[e.selectedIndex].value;

	if (strChk=='2' || strChk=='3' || strChk=='4' || strChk=='9') {
		document.getElementById("preloads").style.visibility = "visible";
		document.getElementById("optionloader").style.display = "table-row";
}
	else {

		document.getElementById("preloads").style.visibility = "hidden";
		document.getElementById("optionloader").style.display = "none";
		document.getElementById("optionloadbox").value = ""; }
}

function populateOptions(x) {

	var defaultOpt=new Array();
	defaultOpt[0] = "0|Off\n1|On";
	defaultOpt[1] = "0|Inactive\n1|Active";
	defaultOpt[2] = "0|False\n1|True";
	defaultOpt[3] = "0|No\n1|Yes";
	document.getElementById("optionloadbox").value = defaultOpt[x];


}

function disableFt(x) {

	var f = document.getElementById("fieldtype");
	var y;
	if (x==0) {
		y = "disabled"; }
	else {
		y = ""; }

	f.options[2].disabled = y;
	f.options[3].disabled = y;
	f.options[4].disabled = y;


}

function dispNewOptionBox() {
	document.getElementById("newoptionval").value = "";
	document.getElementById("newoptiondisp").value = "";
	document.getElementById("newoptbutton").value = "Add";
	document.getElementById("newoptionbox").style.display = "inline";

}

function delOption(x) {
	x_deleteOption(x,rowhide);
	}

function editOption(x) {

	eval("var olddisp = 'disp"+x+"';");
	eval("var oldval = 'val"+x+"';");

	var dispval = document.getElementById(olddisp).innerHTML;
	var optionval = document.getElementById(oldval).innerHTML;

	document.getElementById("addedit").value = x;
	document.getElementById("newoptionval").value = optionval;
	document.getElementById("newoptiondisp").value = dispval;
	document.getElementById("newoptbutton").value = "Edit";

	document.getElementById("newoptionbox").style.display = "inline";

	}

function rowhide(x) {
	eval("var rowval = 'row"+x+"';");
	document.getElementById(rowval).style.display = "none";
	removeOptionArray(x);
}

function stripslashes( str ) {
   // *     example 1: stripslashes('Dave\'s code');
   // *     returns 1: "Dave's code"
   // *     example 2: stripslashes('Dave\\\'s code');
   // *     returns 2: "Dave\'s code"
   return (str+'').replace(/\0/g, '0').replace(/\\([\\'"])/g, '$1');
}

String.prototype.trim = function () {
    return this.replace(/^\s*/, "").replace(/\s*$/, "");
}


function textConvert(val) {
	return stripslashes(decodeURIComponent(val));
}

function addOrEditOption() {

	rowinfo = [];

	rowinfo[0] = document.getElementById("addedit").value;
	rowinfo[1] = document.getElementById("tablename").value;
	rowinfo[2] = document.getElementById("old_column").value;
	rowinfo[3] = document.getElementById("newoptionval").value;
	rowinfo[4] = document.getElementById("newoptiondisp").value;

	var passval = JSON.stringify(rowinfo);

	x_addUpdateOption(passval,showrow);



}

function showrow(x) {

	eval("var rowval = 'val"+x+"';");
	eval("var dispval = 'disp"+x+"';");

	var c = document.getElementById(rowval);


	if (c === null) {

		optionsListArray.push(x);
		var j = "<div id='row"+x+"'><a href='javascript:delOption("+x+");' onclick='return confirm_delete()'>X</a>&nbsp;&nbsp;&nbsp;<a href='javascript:editOption("+x+");'>E</a>&nbsp;&nbsp;&nbsp;<span id='val"+x+"'>"+document.getElementById("newoptionval").value+"</span> - <span id='disp"+x+"'>"+decodeURIComponent(document.getElementById("newoptiondisp").value)+"</span></div>\n";
		var o = document.getElementById("optionblock").innerHTML;
		document.getElementById("optionblock").innerHTML = o+j;
		document.getElementById("newoptionval").value = "";
		document.getElementById("newoptiondisp").value = "";
		document.getElementById("addedit").value = "0";
		document.getElementById("newoptionbox").style.display = "none";

	}

	else {

		document.getElementById(rowval).innerHTML = document.getElementById("newoptionval").value;
		document.getElementById(dispval).innerHTML = decodeURIComponent(document.getElementById("newoptiondisp").value);
		document.getElementById("newoptionval").value = "";
		document.getElementById("newoptiondisp").value = "";
		document.getElementById("addedit").value = "0";
	}

	colorOption();

}

function removeOptionArray(x) {
	var g;
	var k = optionsListArray;
	g = "TEST: "+x+"\n";
	g=g+"LEN: "+k.length+"\n";
	for (var i = 0; i < k.length; i++) {
		g = g+i+": "+optionsListArray[i]+"\n";
		if (optionsListArray[i] == x) {
			optionsListArray[i] = null; }
	}
	colorOption();


}

function colorOption() {
var j = optionsListArray;
var k;
	for (var i = 0; i < j.length; i++) {

		var x = j[i];
			if (x != null) {
			if (k == "#EAEAEA") {
				k = "#FFFFFF"; }
			else {
				k = "#EAEAEA"; }
		var g = "row"+x;
		document.getElementById(g).style.backgroundColor=k;
		}
	}

}

function delRow(table,id) {
	x_delRow(table,id,rowhide);
	}

function delProc(proc) {
	x_delProc(rowhide);
	}


function sortBox(val) {
	var str="<div class='sorttablediv'><div class='tableheadingrow'><div class='celldiv'>Add Sort</div></div>\n";
	str=str+"<div class='sortlistrow'><div class='celldiv'>"+upDownSort('d')+"<a href=\"javascript:addSort('"+val+"',1)\">Sort Ascending</a>";

	str=str+"<BR>"+upDownSort('u')+"<a href=\"javascript:addSort('"+val+"',0)\">Sort Descending</a></div></div></div>";

	document.getElementById("sortbox").innerHTML = str;

	document.getElementById("sortbox").style.visibility = "visible";
}

function addSort(field,sort) {

	for (var i = 0; i < sortList.length; i++) {
		if (sortList[i] != 'nothing') {
			var testarray = sortList[i].split('^');
			if (testarray[0] == field) {
				sortList[i] = "nothing"; }

		}
	}

	var sortmethod;
	if (sort == '1') {
		sortmethod = "^d"; }
	else {
		sortmethod = "^u"; }
	var listlength = sortList.length;
	sortList[listlength]=field+sortmethod;
	adjSortList(sortList);
	showSortList();
	document.getElementById("sortbox").style.visibility = "hidden";


}

function getSortList() {

	if(sortList.length < 1) {
		var uri = window.location.search.split('sort=');
		var val=uri[1];

		if (!val) {
		val = 'id'; }

		var testarray = val.split('|');
		adjSortList(testarray);
	}
	else {
		adjSortList(sortList); }

	showSortList();

}

function saveSort(tablename) {

	var str = "";
	for (var i = 0; i < sortList.length; i++) {
		if (sortList[i] != 'nothing') {
			if (str != '') {
				str=str+"|"; }
			str = str+sortList[i];
		}
	}


	x_saveSort(curtable,str,updateSaved);

}

function updateSaved() {
		document.getElementById("defaultsort").innerHTML = "Default Set!";

}

function adjSortList(check_array) {

	for (var i = 0; i < check_array.length; i++) {
		if (check_array[i] != 'nothing') {
			var testarray = check_array[i].split('^');
			if (testarray[1] == "u") {
				sortList[i] = testarray[0]+"^u"; }
			else {
				sortList[i] = testarray[0]+"^d"; }

		}
	}

}

function showSortList() {


	var vallist = "<div class='sorttablediv'><div class='tableheadingrow'><div class='celldiv'>Sort By&nbsp;&nbsp;(<a href='javascript:resetSort()'>Reset</a>)</div></div>\n";
	vallist = vallist + "<div class='sortlistrow'><div class='sortlistcell'>&nbsp;&nbsp;&nbsp;DEL&nbsp;&nbsp;Field</div></div>\n";

	for (var i = 0; i < sortList.length; i++) {
		if (sortList[i] != 'nothing') {
			var testarray = sortList[i].split('^');
			vallist = vallist + "<div class='sortlistrow'><div class='sortlistcell'>" + upDownSort(testarray[1]);
			vallist = vallist + "<a href='javascript:delSortOption("+i+")'>X</a>&nbsp;&nbsp;\n";
			vallist = vallist + displayFieldName(testarray[0]) + "</div></div>\n";
		}

	}

		vallist=vallist+"<div class='sortlistrow'><div class='celldiv'><a href='javascript:loadSort()'>Do Sort</a>&nbsp;&nbsp;<span id='defaultsort'><a href='javascript:saveSort()'><span id='defaultsort'>Set Default Sort</a></span></div></div></div>\n";


	document.getElementById("sortlist").innerHTML = vallist;


}

function delSortOption(val) {
	sortList[val] = 'nothing';
	adjSortList(sortList);
	showSortList();
}

function loadSort() {
	var str = "";
	for (var i = 0; i < sortList.length; i++) {
		if (sortList[i] != 'nothing') {
			if (str != '') {
				str=str+"|"; }
			str = str+sortList[i];
		}
	}
	var uri = window.location.href.split('?');
	var val=uri[0];
	var finalstr = val+"?table="+curtable+"&sort="+str;
	window.location.href = finalstr;

}

function upDownSort(val) {

	if (val == "u") {
		return "<span class='upsort' title='Sort Ascending'>&uarr;&nbsp;</span>"; }
	else {
		return "<span class='downsort' title='Sort Descending'>&darr;&nbsp;</span>"; }


}

function displayFieldName(val) {
	var z;
	if (val == 'id') {
		z = "ID"; }
	else {
		for (var i = 0; i < fieldNames.length; i++) {
			var testarray = fieldNames[i].split('|');
			if (testarray[0] == val) {
				z = testarray[1];
			}
		}
	}
	return z;

}

function resetSort() {
	var uri = window.location.href.split('?');
	var val=uri[0];
	var finalstr = val+"?table="+curtable+"&sort=id^d";
	window.location.href = finalstr;

}

function showLocInfo(thisInfoArray) {

	for (var i = 0; i < thisInfoArray.length; i++) {
		var tblcell = document.getElementById(thisInfoArray[i]);
		var g = thisInfoArray[i].replace(/-/gi, "_");
		eval("var strid = mi"+g+".id;");
		eval("var strname = mi"+g+".name;");
		eval("var strdesc = mi"+g+".desc;");
		eval("var stricon = mi"+g+".icon;");
		eval("var strloctype = mi"+g+".loctype;");
		eval("var strbarriers = mi"+g+".barriers;");
		eval("var stractions = mi"+g+".lactions;");
			//alert(thisInfoArray[i]);
			tblcell.setAttribute("data-locid", strid);
			tblcell.setAttribute("data-locname", decodeURIComponent(strname));
			tblcell.setAttribute("data-locicon", decodeURIComponent(stricon));
			tblcell.setAttribute("data-loctype", strloctype);
			tblcell.setAttribute("data-locdesc", decodeURIComponent(strdesc));
			tblcell.setAttribute("data-locbarriers", strbarriers);
			tblcell.setAttribute("data-locactions", stractions);

			tblcell.setAttribute("class", "mapcell");
			tblcell.setAttribute("onclick", "getCellinfo(this.id);");
			tblcell.setAttribute("onmouseover", "showLocData(this.id);");
			tblcell.setAttribute("onmouseout", "hideLocData(this.id);");
			var bgtype = setBarrierDisplay( decodeURIComponent(strbarriers)) + tblcell.getAttribute("class")+" loctype"+tblcell.getAttribute("data-loctype");
			tblcell.setAttribute("class",bgtype);

			tblcell.innerHTML = strid;


	}



}

function showLocData(id) {
	var cell = document.getElementById(id);
	var t = cell.getAttribute("data-loctype");
	document.getElementById("loctitle").innerHTML = cell.getAttribute("data-locname");
	document.getElementById("locinfo").innerHTML = "ID: "+cell.getAttribute("data-locid")+" ("+id.replace("x", ", ")+")";
	document.getElementById("loctype").innerHTML = "Location Type: "+decodeURIComponent(locTypeArray[t]);
	var bg = "loctype"+t;
	document.getElementById("loctype").setAttribute("class", "locdatatype "+bg);
	document.getElementById("infobox").style.visibility = "visible";
	overGlowOn(id);
}

function hideLocData(id) {
	document.getElementById("infobox").style.visibility = "hidden";
	overGlowOff(id);
}





function setBarrierDisplay(val) {
	var barClass = "";
	var barrierArray = val.split('|');
		for (var i = 0; i < barrierArray.length; i++) {
			if (barrierArray[i] == '1') {
				var t=i+1;
				barClass = barClass + addBarrierClass(t); }

		}
	return barClass;
}

function addBarrierClass(val) {

	if (val == '1') {
		z="barriernorth "; }
	else if (val == '2') {
		z="barriereast "; }
	else if (val == '3') {
		z="barriersouth "; }
	else if (val == '4') {
		z="barrierwest "; }
	return z;
}





function maploc(id,name,icon,desc,loctype,barriers,lactions) {
	this.name=name;
	this.id=id;
	this.desc=desc;
	this.loctype=loctype;
	this.icon=icon;
	this.barriers=barriers;
	this.lactions=lactions;
}

function showBarriers(barrierArray) {
	j="";
	for (var i = 0; i < barrierArray.length; i++) {
		j=j+barrierText(val)+"<br>";

	}
	return j;
}

function barrierText(val) {
	z="";
	if (val == '1') {
		z="North"; }
	else if (val == '2') {
		z="East"; }
	else if (val == '3') {
		z="South"; }
	else if (val == '4') {
		z="West"; }
	return z;
}

function getCellinfo(val) {
	glowCellOff();
	var cell = document.getElementById(val);
	if (cell.getAttribute("data-locid")) {
		document.getElementById("submitbutton").value = " EDIT "; }

	document.getElementById("form_name").value = decodeURIComponent(cell.getAttribute("data-locname"));
	document.getElementById("form_icon").value = decodeURIComponent(cell.getAttribute("data-locicon"));
	document.getElementById("form_loctype").value = cell.getAttribute("data-loctype");
	document.getElementById("form_description").innerHTML = decodeURIComponent(cell.getAttribute("data-locdesc"));
	if (cell.getAttribute("data-locicon")) {
		document.getElementById("icondiv").innerHTML = "<a href='/images/loc/"+decodeURIComponent(cell.getAttribute("data-locicon"))+".jpg' target='_new'><img src='/images/loc/"+decodeURIComponent(cell.getAttribute("data-locicon"))+".jpg' class='iconthumb'></a>";
	}

	var t = decodeURIComponent(cell.getAttribute("data-locbarriers"));
	document.getElementById("loc_id").innerHTML = cell.getAttribute("data-locid");
	document.getElementById("form_id").value = cell.getAttribute("data-locid");
	mapActions(cell.getAttribute("data-locactions"));
	var coord = getCoordinates(val);
	document.getElementById("form_x").value = coord[0];
	document.getElementById("form_y").value = coord[1];
	setBarrierInfo(cell.getAttribute("data-locid"),t,val);
	glowCellOn(val);



}

function overGlowOn(val) {
	globalSetAttribute(val,"class",globalGetAttribute(val,"class").AddClass("overglow"));
}

function overGlowOff(val) {
	globalSetAttribute(val,"class",globalGetAttribute(val,"class").RemoveClass("overglow"));
}

function glowCellOn(val) {
	globalSetAttribute(val,"class",globalGetAttribute(val,"class").AddClass("glowcell"));
}

function glowCellOff() {
	var x=document.getElementById("form_x").value;
	var y=document.getElementById("form_y").value;
	if (x && y) {
		var val = x+"x"+y;
		globalSetAttribute(val,"class",globalGetAttribute(val,"class").RemoveClass("glowcell")); }
}



function getCoordinates(val) {
	var coordarray = val.split('x');
	return coordarray;

}

function returnBarrierStyle(val) {
	var z;
	if (!val) {
		z = "error"; }
	else if (val == "0") {
		z = "barrieroff"; }

	else if (val == "1") {
		z = "barrieron"; }

	else {
		z = "barrierspecial"; }
	return z;
}

function setBarrier(barclass,id,direction,barrier,xyid) {

	var x="changeBarriers("+id+",'"+direction+"','"+barrier+"','"+xyid+"');";
	document.getElementById(direction).setAttribute("class", barclass);
	document.getElementById(direction).setAttribute("data-barrier", barrier);
	document.getElementById(direction).setAttribute("onclick", x);

}

function changeBarriers(id,direction,val,xyid) {
	if (val=='1') {
		var p = '0'; }
	else {
		var p='1'; }

	document.getElementById(direction).setAttribute("data-barrier", p);
	barrierArray = stitchBarrierInfo();
	var passval = JSON.stringify(barrierArray);
	var g = barrierArray[0]+"|"+barrierArray[1]+"|"+barrierArray[2]+"|"+barrierArray[3];
	var tblcell = document.getElementById(xyid);

	tblcell.setAttribute("data-locbarriers", g);
	setBarrierInfo(id,g,xyid);
	var bgtype = "mapcell "+setBarrierDisplay( decodeURIComponent(g)) +" loctype"+tblcell.getAttribute("data-loctype");
	tblcell.setAttribute("class",bgtype);
	x_updateBarriers(id,g,refreshBarriers);
}

function stitchBarrierInfo() {
	var barrierArray = new Array();
	barrierArray[0] = document.getElementById("barrier_n").getAttribute("data-barrier");
	barrierArray[1] = document.getElementById("barrier_e").getAttribute("data-barrier");
	barrierArray[2] = document.getElementById("barrier_s").getAttribute("data-barrier");
	barrierArray[3] = document.getElementById("barrier_w").getAttribute("data-barrier");

	return barrierArray;
}

function setBarrierInfo(cellid,val,xyid) {

	var barrierarray = val.split('|');
	var directionArray = ['barrier_n','barrier_e','barrier_s','barrier_w'];

	for (var i = 0; i < barrierarray.length; i++) {
		var g=returnBarrierStyle(barrierarray[i]);
		setBarrier(g,cellid,directionArray[i],barrierarray[i],xyid);
	}
}

function refreshLoad(preval) {
	var id = preval.split('|');
	var uri = window.location.href.split('?');
	if (id[1]) {
		var x = uri[0]+"?locload="+id[1]; }
	else {
			var x = uri[0]+"?locload="+uri[1]; }
	window.location.href = x;


}

function refreshBarriers() {
//

}

function addUpdateCellData() {

	var passArray = new Array();
	passArray[0] = document.getElementById("form_id").value;
	passArray[1] = document.getElementById("form_x").value;
	passArray[2] = document.getElementById("form_y").value;
	passArray[3] = document.getElementById("form_name").value;
	passArray[4] = document.getElementById("form_description").value;
	barrierArray = stitchBarrierInfo();
	passArray[5] = barrierArray[0]+"|"+barrierArray[1]+"|"+barrierArray[2]+"|"+barrierArray[3];
	passArray[6] = document.getElementById("form_loctype").value;
	passArray[7] = document.getElementById("form_icon").value;
	passArray[8] = buildActionList();

	var g = JSON.stringify(passArray);
	x_addUpdateCellData(g,refreshLoad);


}

function buildActionList() {
	var actionArray = document.getElementById("form_actions").options;
	var g = "";
	for (var i = 0; i < actionArray.length; i++) {
		if (actionArray[i].selected) {
			if (g.length > 0) { g=g+"|"; }
			g=g+actionArray[i].value;
		}
	}

	return g;
}

function addLocTypeSelect() {
	var x="<select name='form_loctype' id='form_loctype'>\n";

	var cell = document.getElementById("typespan");
	for (var i = 0; i < locTypeArray.length; i++) {
		x=x+"<option value='"+i+"'>"+locTypeArray[i]+"\n";
	}

	x=x+"</select>\n";

	cell.innerHTML = x;
}

function mapBuildWarningOn() {
	var mapwarning = document.getElementById("mapbuild");
	mapwarning.innerHTML = "Current version of map file is NOT built. Click here to build.";
	mapwarning.setAttribute("class", "mapwarningon");
	mapwarning.setAttribute("onclick", "buildMapFile();");


}

function mapBuildWarningOff() {
	var mapwarning = document.getElementById("mapbuild");
	mapwarning.innerHTML = "Current version of map file is built.";
	mapwarning.setAttribute("class", "mapwarningoff");
}

function mapBuild(str) {

	if (str != 1) {
		mapBuildWarningOn(); }
	else {
		mapBuildWarningOff(); }

}

function buildMapFile() {
	window.location.href = "/admin/config_builder/";
}

String.prototype.RemoveClass = function (val) {
    return this.replace(val, "");
}

function mapActions(actionslist) {
	clearMapActions();
	var actionelement = document.getElementById("form_actions").options;
	var actionsarray = actionslist.split('|');
	for (var g = 0; g < actionsarray.length; g++) {
		for (var i = 0; i < actionelement.length; i++) {
			if (actionelement[i].value == actionsarray[g]) {
				actionelement[i].setAttribute("selected", "selected");
			}
		}
	}
	document.getElementById("form_actions").focus();
}

function clearMapActions() {
	var actionelement = document.getElementById("form_actions").options;
	for (var g = 0; g < actionelement.length; g++) {
		actionelement[g].removeAttribute("selected");
	}


}

function triggerIDLoad(loc) {
	document.getElementById("infobox").focus();
	setTimeout(getCellinfo(loc), 2000);

}



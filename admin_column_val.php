<?php

include("includes/includes_nohead.php");

$tablename = $_POST['tablename'];
$column = $_POST['column'];
$coltype = $_POST['coltype'];
$oldcolumn = $_POST['old_column'];
$selectedval = $_POST['selectedval'];
$inputtype = isUnset($_POST['inputtype']);
$comment = $_POST['comment'];
$defaultval = $_POST['default'];
$visibleadmin = isUnset($_POST['visibleadmin']);

$dispval = urlencode($comment) . "|" . $inputtype . "|" . $selectedval . "|" . $visibleadmin;

$sql = "ALTER TABLE " . $tablename . " CHANGE " . $oldcolumn . " " . $column . " " . $coltype . " COMMENT '" . $dispval . "' DEFAULT '" . $defaultval . "'";

runSQL($sql);

echo $sql;

$sql = "UPDATE " . ADMIN_TABLES_OPTIONS . " SET tbl_column='" . $column . "' WHERE tbl_name='" . $tablename . "' AND tbl_column='" . $oldcolumn . "'";

runSQL($sql);

header('location: admin_table.php?table=' . $tablename . '&field=' . $column);

?>


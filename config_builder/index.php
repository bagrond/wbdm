<?php
include("../includes/header.php");

function configType($val) {
	if ($val == "1") {
		return "Server"; }
	elseif ($val == "2") {
		return "Client"; }
	elseif ($val == "3") {
		return "Text"; }
	else {
		return "UNKNOWN!"; }
}

echo "<h3>Config File Builder</h3>\n";

echo "<div class='tablediv'>\n";
echo "<div class='rowhead'><div class='celldiv'>Config File</div><div class='celldiv'>Type</div><div class='celldiv'>Current Version</div><div class='celldiv'>Is Current</div><div class='celldiv'>Build Config</div></div>\n";

	$sql = "SELECT id,config_file_name, config_type,config_file_version,config_is_current FROM config_builder ORDER BY config_file_name ASC";

	$con = mysql_connect(CONN_SERVER,CONN_USER,CONN_PWD);
	mysql_select_db(CONN_DB, $con);
	$rs = mysql_query($sql);
	while ($row = mysql_fetch_assoc($rs)) {

		$bgcolor = rowColor($bgcolor);
		echo "<div class='" . $bgcolor . " rowdiv'><div class='celldiv'>\n";
		echo "<a href='/admin/addedit_row.php?table=config_builder&id=" . $row['id'] . "'>" . $row['config_file_name'] . "</a></div>\n";
		echo "<div class='celldiv'>" . configType($row['config_type']) . "</div>\n";
		echo "<div class='celldiv'>" . $row['config_file_version'] . "</div>\n";
		echo "<div class='celldiv'>" . YesNo($row['config_is_current']) . "</div>\n";
		echo "<div class='celldiv'><a href='buildconfig.php?id=" . $row['id'] . "'>Build</a></div></div>\n";

	}


	mysql_close($con);

echo "</div>\n";



include("../includes/footer.php");

?>


<?php

include("includes/header.php");



$tablename = $_GET['table'];
$paging = $_GET['paging'];

if (!$paging) {
	$paging = "0"; }
$x=0;
$z=0;

$getfields = "";
$fieldnames = "";
$cursort = $_GET['sort'];

$sortlist = getDefaultSort($cursort,$tablename);

$sort = setSort($sortlist);
$sortjs = setSortJS($sortlist);

$recordlimit = " LIMIT " . ($paging * RECORD_LIMIT) . "," . RECORD_LIMIT;

echo "<script>var curtable='". $tablename . "'; var sortList=new Array();var fieldNames=new Array();" . $sortjs . "</script>\n";

echo "<a name='add'></a><H2>View All Records - " . $tablename . "</H2>";


echo "<a href='addedit_row.php?table=" . $tablename . "'>Add Record</a><br><br>\n";
echo "<a href='delete_row.php?page=viewall&id=all&table=" . $tablename . "' onclick='return confirm_delete()'>Delete ALL Records</a><br><br>\n";

echo "<div class='entrytablediv'>\n";
echo "<div class='rowdiv'>\n";

echo "<div class='celldiv' id='sortlist'>Sort List</div>";


echo "<div class='sortbox celldiv' id='sortbox'>Sort Box</div>";

echo "</div>\n";
echo "</div>\n";


echo "<div class='tablediv'><div class='rowhead'>\n<div>DEL</div>\n";
echo "<div class='celldiv'><a href=\"javascript:sortBox('id')\">ID</a></div>\n";

$fields = getFieldInfo($tablename);

foreach($fields as $str) {
	if ($str[4]) {


		if (hasOptions($str[2])) {

			$chk = $tablename . "_" . $str[0];
				if (!$_SESSION[$chk]) {
					setOptionsSession($tablename,$str[0]);
				}
			$chk = null;

		}

		if ($str[1]) {
			$fieldhead = $str[1];
		}
		else {
			$fieldhead = $str[0];
		}

		echo "<div class='celldiv'><a href=\"javascript:sortBox('" . $str[0] . "')\">" . $fieldhead . "</a></div>\n";
		$fieldnames .= "fieldNames[" . $z . "]='" . $str[0] . "|" . $str[1] . "';\n";
		$z++;
		if ($getfields) {
			$getfields .= ","; }

		$getfields .= $str[0];
	}

}

echo "</div>\n";

$sql = "SELECT id," . $getfields . " FROM " . $tablename . " ORDER BY " . $sort . " " . $recordlimit;

global $con;

if (!$con) {
  die('Could not connect: ' . mysqli_error()); }

  $rs = mysqli_query($con,$sql);

while ($row = mysqli_fetch_assoc($rs)) {
	$x++;

	$rowid = $row['id'];

	$bgcolor = rowColor($bgcolor);

	foreach($row as $key=>$value) {


		if ($key != "id") {
			$chk = $tablename . "_" . $key;
			if ($_SESSION[$chk][$value]) {
				$thisval = $_SESSION[$chk][$value]; }
			else {
				$thisval = $value; }

			echo "<div class='celldiv'>" . strip_tags(substr(textDecode($thisval), 0, 70)) . "</div>\n"; }

		else {
				echo "<div class='" . $bgcolor . " rowdiv' id='row" . $rowid . "'>\n<div class='celldiv'><a href=\"javascript:delRow('" . $tablename . "'," . $value  . ");\" onclick=\"return confirm_delete()\">X</a></div>\n";
				echo "<div class='celldiv'><a href='addedit_row.php?table=" . $tablename . "&id=" . $value . "'>" . $value . "</a></div>\n"; }


		}
	echo "</div>\n";

}

	echo "</div>\n";

echo "<script>" . $fieldnames . " getSortList();</script>\n";




if ($x >= (RECORD_LIMIT-1)) {
	echo "<a href='view_all.php?table=" . $tablename . "&paging=" . $paging . "'>Results " . (($paging * RECORD_LIMIT)+1) . " - " . (($paging+1) * RECORD_LIMIT) . "</a>&nbsp;&nbsp;";

	$paging++;
	echo "<a href='view_all.php?table=" . $tablename . "&paging=" . $paging . "'>Results " . (($paging * RECORD_LIMIT)+1) . " - " . (($paging+1) * RECORD_LIMIT) . "</a><br><br>";
}

echo "<br><a href='#add'>Return to top</a>";




include("includes/footer.php");

?>



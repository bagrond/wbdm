<?php
include("includes/constants.php");
include("includes/functions.php");

$key = $_POST['key'];
$dbfile = "dbinfo.php";
$dbpath = "includes/";


	$str = "<?php \n";

	$str .= "# ************* DATABASE CONSTANTS *************\n\n";
	$str .= "#  Created on: " . date("F j, Y, g:i a") . "\n";
	$str .= "#  Created using Dave's Game DB Manager Version " . CUR_VER . "\n\n";
	$str .= "  define('CONN_DB', \"" . $_POST['dbname'] . "\");\n";
	$str .= "  define('CONN_PWD', \"" . $_POST['dbpwd'] . "\");\n";
	$str .= "  define('CONN_SERVER', \"" . $_POST['dbserver'] . "\");\n";
	$str .= "  define('CONN_USER', \"" . $_POST['dbuser'] . "\");\n\n";

	$str .= "# *********** END DATABASE CONSTANTS ***********\n\n";


	$str .= "?>";

	$retval = createFile($dbfile,$dbpath,$str);

header('location: generate_admintable.php');

?>

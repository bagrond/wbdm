<?php

include("includes/includes_nohead.php");


$tablename = trim($_POST['tablename']);
$columnname = trim(strtolower($_POST['name']));
$columntype = $_POST['column'];
$coment = $_POST['comment'];
$default = $_POST['default'];
$fieldtype = $_POST['fieldtype'];
$seloption = $_POST['selectedoption'];
$visible = $_POST['visibleadmin'];
$options = trim($_POST['optionloadbox']);

$stitch = urlencode(trim($coment)) . "|" . $fieldtype . "|" . $seloption . "|" . $visible;

if ($columntype == '0') {
	$val = "TINYINT(1)"; }
elseif ($columntype == '1') {
	$val = "DATE"; }
elseif ($columntype == '2') {
	$val = "DATETIME"; }
elseif ($columntype == '3') {
	$val = "INT(11)"; }
elseif ($columntype == '4') {
	$val = "INT(6)"; }
elseif ($columntype == '5') {
	$val = "MEDIUMTEXT"; }
elseif ($columntype == '6') {
	$val = "INT(3)"; }
elseif ($columntype == '7') {
	$val = "VARCHAR(" . $_POST['size'] . ")"; }
elseif ($columntype == '8') {
	$val = "TIMESTAMP"; }



$sql = "ALTER TABLE " . $tablename . " ADD COLUMN " . $columnname . " " . $val. " COMMENT '" . $stitch . "' DEFAULT '" . urlencode(trim($default)) . "'";

runSQL($sql);

if ($options) {
	runSQL(addSelectionOptions($options,$tablename,$columnname)); }

header('location: admin_table.php?table=' . $tablename);


?>
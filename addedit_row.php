<?php

$titleval = "Add/Edit Entry";
include("includes/header.php");


$tablename = $_GET['table'];
$rowid = $_GET['id'];
$paging= $_GET['paging'];
$build = array();

$fields = getFieldInfo($tablename);


if ($rowid) {
	$sql = "SELECT * FROM " . $tablename . " WHERE id=" . $rowid;

	global $con;

	if (!$con) {
	  die('Could not connect: ' . mysqli_error()); }
	
	  $rs = mysqli_query($con,$sql);
	
	while ($row = mysqli_fetch_assoc($rs)) {
		foreach($row AS $key => $value) {
			foreach($fields as $str) {
				if ($str[0] == $key) {
					$buildname = $tablename . "_" . $key;

					if (isBool($str[2])) {
						$build[$buildname] = $str[5] . "|" . $value;

					}
					else {
						$build[$buildname] = $value; }

				}

			}

		}


	}
}
echo "<form method='POST' action='addedit_row_val.php'>\n";
echo "<input type='hidden' name='id' value='" . $rowid . "'>\n";
echo "<input type='hidden' name='tablename' value='" . $tablename . "'>\n";
echo "<table>";

foreach($fields as $str) {
	if ($str[0] == "id") {
		echo "<tr><td class='fieldheading'>ID:</td><td>";
		echo $rowid;
		echo "</td></tr>";
	}
	else {
			if (!$str[1]) {
				echo "<tr><td class='fieldheading'>" . textDecode($str[0]) . "</td><td>";
			}
			else {
				echo "<tr><td class='fieldheading'>" . textDecode($str[1]) . "</td><td>";
			}
		echo showFormField($tablename,$str[0],$str[2],$build);
		echo "</td></tr>";
			
	}
}

echo "<tr><td colspan='2'><input type='submit' value=' DO IT '></td></tr>\n";
echo "</table>\n";
echo "</form>\n";




include("includes/footer.php");

?>



<?php

include("includes/header.php");

$tablename = $_GET['table'];

echo "<H5>Table Administration - " . $tablename . "</H5>";

echo "<div class='tablediv'>\n";
echo "<div class='rowhead'><div class='celldiv'>DEL</div>\n";
echo "<div class='celldiv'>ADMIN</div>\n";
echo "<div class='celldiv'>Key</div>\n";
echo "<div class='celldiv'>Field</div>\n";
echo "<div class='celldiv'>Type</div>\n";
echo "<div class='celldiv'>Default</div>\n";
echo "<div class='celldiv'>Extra</div>\n";
echo "<div class='celldiv'>Description</div>\n";
echo "<div class='celldiv'>Input Type</div>\n";
echo "<div class='celldiv'>Visible</div></div>\n";


showAllColums($tablename);

echo "</div>\n";



echo "<div class='entrytablediv'>\n";
echo "<div class='tableheadingrow'>\n";
echo "<div class='tableheadingcell'>Add Column</div></div>\n";


echo "<form method='post' action='add_column.php'>\n";
echo "<input type='hidden' name='tablename' value='" . $tablename . "'>\n";

echo "<div class='rowdiv'><div class='helpldiv'><img src='images/question.png' border='0' title='" . $helptext[3] . "'> Name:</div>";
echo "<div class='celldiv'><input type='text' name='name' size='20'></div></div>\n";
echo "<div class='rowdiv'><div class='helpldiv'><img src='images/question.png' border='0' title='" . $helptext[6] . "'> Display Text:</div><div class='celldiv'><input type='text' name='comment' size='20'></div></div>\n";

echo "<div class='rowdiv'><div class='helpldiv'><img src='images/question.png' border='0' title='" . $helptext[2] . "'> Field Type:</div>";
echo "<div class='celldiv'><select name='column' id='coltype' onchange='varcharsize();'>\n";

displayOptions(1);

echo "</select></div></div>\n";
echo "<div class='rowdiv' id='varcharsize'><div  class='helpldiv'><img src='images/question.png' border='0' title='" . $helptext[4] . "'> Size:</div>";
echo "<div class='celldiv'><input type='text' name='size' size='5'></div></div>\n";
echo "<div class='rowdiv'><div class='helpldiv'><img src='images/question.png' border='0' title='" . $helptext[5] . "'> Input Type:</div>\n";
echo "<div class='celldiv'><select name='fieldtype' id='fieldtype' onchange='optionsEntryField();'>\n";


displayOptions(2);

echo "</select></div></div>\n";

echo "<div class='rowdiv'><div class='helpldiv'><img src='images/question.png' border='0' title='" . $helptext[7] . "'> Default Value:</div>";
echo "<div class='celldiv'><input type='text' name='default' size='20'></div></div>\n";
echo "<div class='rowdiv'><div class='helpldiv'><img src='images/question.png' border='0' title='" . $helptext[8] . "'> Visible on View Page:</div><div class='celldiv'><input type='checkbox' name='visibleadmin' value='1' CHECKED></div></div>\n";
echo "<div class='rowdiv' id='optionloader'><div class='helpldiv'><img src='images/question.png' border='0' title='" . $helptext[3] . "'> Selection Options:</div>";
echo "<div class='celldiv'><textarea name='optionloadbox' id='optionloadbox' cols='40' rows='10'></textarea><br>Selected Value: <input name='selectedoption' size='5' type='text'></div>\n";

echo "<div class='celldiv' id='preloads'>Pre-Loads<br><a href='javascript:populateOptions(0);'>On/Off</a><br><br><a href='javascript:populateOptions(1);'>Active/Inactive</a><br><br><a href='javascript:populateOptions(2);'>True/False</a><br><br><a href='javascript:populateOptions(3);'>Yes/No</a></div><\n";
echo "</div>\n";
echo "<div class='rowdiv'><div class='celldiv'><input type='submit' value=' ADD COLUMN '></div></div>\n";

echo "</div>\n";
echo "</form>\n";






include("includes/footer.php");

?>



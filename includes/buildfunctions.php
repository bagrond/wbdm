<?php

function determineBuild($row,$addedit) {

	if ($row['fieldtype'] == 'text') {
		$str = buildText($row,$addedit); }
	elseif ($row['fieldtype'] == 'textarea') {
		$str = buildTextarea($row,$addedit); }
	elseif ($row['fieldtype'] == 'checkbox') {
		$str = buildCheckbox($row,$addedit); }
	elseif ($row['fieldtype'] == 'boolean') {
		$str = buildCheckbox($row,$addedit); }
	elseif ($row['fieldtype'] == 'date') {
		$str = buildDate($row,$addedit); }
	elseif ($row['fieldtype'] == 'datetime') {
		$str = buildDatetime($row,$addedit); }
	elseif ($row['fieldtype'] == 'timestamp') {
		$str = buildDatetime($row,$addedit); }
	elseif ($row['fieldtype'] == 'numerictext') {
		$str = buildText($row,$addedit); }

	return $str;
}

function buildText($row,$addedit) {

	if ($addedit) {
		$val = "value='" . $row['rowvalue'] . "'"; }

	$str = "<tr><td><b>" . urldecode($row['displaytext']) . "</b></td>\n";
	$str .= "<td><input type='text' name='" . $row['fieldname'] . "' size='" . $row['size'] . "' " . $val . "></td></tr>\n";
	return $str;

}

function buildTextarea($row,$addedit) {
	if ($addedit) {
		$val = $row['rowvalue']; }
	$str = "<tr><td><b>" . urldecode($row['displaytext']) . "</b></td>\n";
	$str .= "<td><textarea cols='40' rows='10' name='" . $row['fieldname'] . "'>" . $val . "</textarea></td></tr>\n";
	return $str;

}

function buildCheckbox($row,$addedit) {

	if ($addedit && ($row['value'] == $row['rowvalue'])) {
		$val = " CHECKED"; }

	$str = "<tr><td><b>" . urldecode($row['displaytext']) . "</b></td>\n";
	$str .= "<td><input type='checkbox'  name='" . $row['fieldname'] . "' value='" . $row['value'] . "'" . $val . "></td></tr>\n";
	return $str;

}

function buildDate($row,$addedit) {

	if ($addedit) {
		$val = "value='" . $row['rowvalue'] . "'"; }
	else {
		$val = "value='" . date("Y-m-d") . "'"; }

	$str = "<tr><td><b>" . urldecode($row['displaytext']) . "</b></td>\n";
	$str .= "<td><input type='text' name='" . $row['fieldname'] . "' size='20' " . $val . "></td></tr>\n";
	return $str;

}

function buildDatetime($row,$addedit) {

	if ($addedit) {
		$val = "value='" . $row['rowvalue'] . "'"; }
	else {
		$val = "value='" . date("Y-m-d H:i:s") . "'"; }

	$str = "<tr><td><b>" . urldecode($row['displaytext']) . "</b></td>\n";
	$str .= "<td><input type='text' name='" . $row['fieldname'] . "' size='20' " . $val . "></td></tr>\n";
	return $str;

}



?>
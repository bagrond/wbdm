<?php

# ========================================================
#	CONSTANTS FOR DB MANAGER
#	Developed by Dave Baker for web development to automate tedious
#	system development for the myriad projects I start and then run into a wall
#	because of things like this.
#
#	Current Version: v1.4
#	Last updated: 11/12/18
#
# -------------------------------------------------------
#	Change Log - Note and Major Changes
#
# -------------------------------------------------------
#
# ========================================================

define('CUR_VER', "1.0");
define('DBMANAGER_TABLE', "admin_tables");
define('CONFIG_TABLE', "config_builder");
define('ADMIN_TABLES_OPTIONS', "admin_tables_options");
define('ADMIN_TABLES_SORTS', "admin_tables_sorts");
define('ADMIN_PREFIX', "admin_");
define('COL_TYPE_COUNT', "8");
define('FIELD_TYPE_COUNT', "8");
define('RECORD_LIMIT', "100");
define('INC_PATH', "/srl/admin/db/includes/");
define('ROOT_PATH', "/srl/admin/db/");
define('UI_PATH', "ui/");
define('JS_PATH', "js/");
define('WEBROOT', "www");


define('MAP_TYPE_FIELD', "location_type");
define('MAP_TABLE', "map");

define('COL_NAMES', serialize (array("Boolean", "Date", "Date-Time", "Integer", "Medium Integer", "Medium Text", "Small Integer", "Varchar", "Timestamp")));
define('FIELD_NAMES', serialize (array("Text", "Text Area", "Selection", "Checkbox", "Boolean", "Date", "Date-Time", "Numeric Text", "Timestamp", "Multiple Selection")));


define('COL_TYPE_BOOL', "0");
define('COL_TYPE_DATE', "1");
define('COL_TYPE_DT', "2");
define('COL_TYPE_INT', "3");
define('COL_TYPE_MINT', "4");
define('COL_TYPE_MTXT', "5");
define('COL_TYPE_SINT', "6");
define('COL_TYPE_VCHR', "7");
define('COL_TYPE_TSMP', "8");

define('FIELD_TYPE_TEXT', "0");
define('FIELD_TYPE_TARE', "1");
define('FIELD_TYPE_SELE', "2");
define('FIELD_TYPE_CKBX', "3");
define('FIELD_TYPE_BOOL', "4");
define('FIELD_TYPE_DATE', "5");
define('FIELD_TYPE_DT', "6");
define('FIELD_TYPE_NTXT', "7");
define('FIELD_TYPE_TSMP', "8");


?>
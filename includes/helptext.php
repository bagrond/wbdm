<?php

$helptext = array();

// Table Name
	$helptext[0] = "Name of the table in the database. Lowercase, alphanumeric with no spaces.";

// Table Description
	$helptext[1] = "Display text of the table name.";

// Column Type
	$helptext[2] = "Type of column. SmallINT=3, MediumINT=6, INT=11";

// Column Name
	$helptext[3] = "Name of the column in the table. Lowercase, alphanumeric with no spaces.";

// Column Size
	$helptext[4] = "Relevant for VARCHAR only. Max length of the VARCHAR field.";

// Field Type
	$helptext[5] = "Field type, different than column type.";

// Display Text
	$helptext[6] = "Field name to display in table headings";

// Default Value
	$helptext[7] = "Default value, if any. It must be the proper type.";


// Visible on Admin Page
	$helptext[8] = "Check if it should be visible on the View All page.";

//
	$helptext[9] = "";

//
	$helptext[10] = "";
?>
<?php

function buildConfigFile($buildid) {

	$sql = "SELECT * FROM config_builder WHERE id=" . $buildid;

	$actionrow = getRecordDBCall($sql);

	$GLOBALS["configtype"] = $actionrow['config_type'];

	$con = mysql_connect(CONN_SERVER,CONN_USER,CONN_PWD);
	$fileval = configHeader();
	$fileval .= configBuild("========================================================\n");
	$fileend .= configFooter();

	if (trim($actionrow['config_reqs'])) {
		$reqs = urldecode($actionrow['config_reqs']); }
	else {
		$reqs = "ORDER BY id ASC"; }

	$ver = "Version: " . ($actionrow['config_file_version']+1) . "\n\n";
	$fileval .= configBuild("Config file: ") . $actionrow['config_file_name'] . "(" . $actionrow['config_table'] . ")\n";
	$fileval .= configBuild("Built: ") . date("F j, Y, g:i a") . "\n";
	$fileval .= configBuild($ver);
	$fileval .= configBuild("Elements:\n");


	$fields = getFieldInfo($actionrow['config_table']);

	foreach($fields as $str) {
		$fileval .= configBuild("  " . $str[1] . " (" . $str[0] . ")\n");
	}

	$fileval .= "\n" . configBuild("Notes:\n\n");

	$confignotes = explode("\n",urldecode($actionrow['config_notes']));

	foreach ($confignotes AS $str) {
		$fileval .= configBuild($str ."\n");
	}

	$fileval .= configBuild("========================================================\n\n");

	$fileval .= urldecode($actionrow['config_header']) . "\n\n";

	$filename .= $actionrow['config_file_name'] . configFileType();
	$path = dirname($_SERVER['DOCUMENT_ROOT']) . DIRECTORY_SEPARATOR . WEBROOT . DIRECTORY_SEPARATOR . $actionrow['config_file_dir'] . DIRECTORY_SEPARATOR;
	
	$loc = $path . $filename;

	mysql_select_db(CONN_DB, $con);


	$sql = "SELECT * FROM " . $actionrow['config_table'] . "  " . $reqs;

	$rs = mysql_query($sql);
	while ($row = mysql_fetch_assoc($rs)) {
		$basetemplate = urldecode($actionrow['config_line_template']);
		$comarray = explode("\n",$basetemplate);
		foreach ($comarray AS $template) {

			$template = trim(urldecode($template));
			if (strlen($template) > 0) {
				eval($template); }

		}
	}


	mysql_close($con);

	$fileval .= urldecode($actionrow['config_footer']) . "\n\n";


	$fileval .= $fileend;

	createFile($filename,$path,$fileval);
	
	return $loc;
}

function configFileType() {
	global $configtype;
	if ($configtype == "1") {
		return ".php"; }
	elseif ($configtype == "2") {
		return ".js"; }
	else {
		return ".txt"; }
}



function configFilePath() {
	global $configtype;
	if ($configtype == "1") {
		return ".php"; }
	else {
		return ".js"; }
}

function configComment() {
	global $configtype;
	if ($configtype == "1") {
		return "# "; }
	elseif ($configtype == "2") {
		return "// * "; }
	else {
		return "# "; }
}

function configHeader() {
	global $configtype;
	if ($configtype == "1") {
		return "<?php \n\n"; }
	else {
		return ""; }
}

function configFooter() {
	global $configtype;
	if ($configtype == "1") {
		return "?>"; }
	else {
		return ""; }
}

function configBuild($val) {
	global $configtype;
	return configComment($configtype) . $val; }



?>
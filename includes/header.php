<?php

session_start();
ob_start();

	//define base path prior to install
	$base_path =  "srl" .  DIRECTORY_SEPARATOR . "admin" .  DIRECTORY_SEPARATOR . "db";
	define('WEBROOT', "www");
	$dbinfo = "dbinfo.php";

$path = dirname($_SERVER['DOCUMENT_ROOT']) . DIRECTORY_SEPARATOR . WEBROOT . DIRECTORY_SEPARATOR . $base_path . DIRECTORY_SEPARATOR;
$includes_path = $path . "includes" . DIRECTORY_SEPARATOR;

require_once($path . "env_settings.php");

require_once($includes_path . "sajax.php");

//$sajax_debug_mode = true;
sajax_export(
	array("name" => "deleteOption"),
	array("name" => "delRow"),
	array("name" => "saveSort"),
	array("name" => "updateBarriers"),
	array("name" => "addUpdateCellData"),
	array("name" => "buildMapInclude"),
	array("name" => "addUpdateOption")
);
sajax_handle_client_request();


$header = "<HTML>\n  <HEAD>\n";
$header .= "<TITLE>State Resource Locator Management Tool</TITLE>\n";
$header .= "<script src='" . ROOT_PATH . JS_PATH. "js_functions.js'></script>\n";
$header .= "<link rel='stylesheet' type='text/css' href='" . ROOT_PATH . UI_PATH ."style.css' />\n";

if ($altcss) {
	$header .= "<link rel='stylesheet' type='text/css' href='" . ROOT_PATH . UI_PATH . $altcss . ".css' />\n";
}

$header .= "</HEAD>\n<BODY>\n";
$header .= "<H3>State Resource Locator Database Management Tool</H3>\n";

echo $header;

echo "<script type='text/javascript' src='" . INC_PATH . "json2.stringify.js'></script>\n";
echo "<script type='text/javascript' src='" . INC_PATH . "json_stringify.js'></script>\n";
echo "<script type='text/javascript' src='" . INC_PATH . "json_parse_state.js'></script>\n";
echo "<script type='text/javascript' src='" . INC_PATH . "sajax.js'></script>\n";
?>

<script type="text/javascript"><!--
	<?php sajax_show_javascript(); ?>
//-->
</script>


<?php

//MySQL management functions

$con = new mysqli(CONN_SERVER, CONN_USER, CONN_PWD, CONN_DB);

function runSQL($sql) {

	global $con;

	if (!$con) {
	  die('Could not connect: ' . mysqli_error()); }

	mysqli_query($con,$sql);

}


function singleDBCall($field,$sql) {

	global $con;

	if (!$con) {
	  die('Could not connect: ' . mysqli_error()); }

		$result = mysqli_query($con,$sql);

		$row = $result->fetch_assoc();

		return $row[$field];

}

function getRecordDBCall($sql) {

	global $con;

	if (!$con) {
	  die('Could not connect: ' . mysqli_error()); }

		$result = mysqli_query($con,$sql);

		$row = $result->fetch_assoc();

		return $row;

}

function showAllTables() {

	$sql = "SHOW TABLE STATUS FROM " . CONN_DB;

	global $con;

	if (!$con) {
	  die('Could not connect: ' . mysqli_error()); }

	  $rs = mysqli_query($con,$sql);

	while ($row = mysqli_fetch_assoc($rs)) {

		if (!findAdminTables($row['Name'])) {

			$bgcolor = rowColor($bgcolor);


			echo "<tr class='" . $bgcolor . "'><td><a href='drop_table.php?table=" . $row['Name'] . "' onclick='return confirm_delete()'><img src='images/delete.png' border='0' title='Drop Table'></a></td>\n";
			echo "<td><a href='admin_table.php?table=" . $row['Name'] . "'><img src='images/admin.png' border='0' title='Administer'></a></td>\n";
			echo "<td><a href='view_all.php?table=" . $row['Name'] . "'>" .  urldecode($row['Name']) . "</a></td>\n";
			echo "<td>" .  urldecode($row['Comment']) . "</td></tr>\n";
		}
	}


	mysqli_close($con);
}

function showAllColums($tablename) {

	$sql = "SHOW FULL COLUMNS FROM " . $tablename;

	global $con;

	if (!$con) {
	  die('Could not connect: ' . mysqli_error()); }

	  $rs = mysqli_query($con,$sql);

	  while ($row = mysqli_fetch_assoc($rs)) {
		  $bgcolor = rowColor($bgcolor);
	  
		  if ($row['Field'] != 'id') {
	  
			  echo "<div class='" . $bgcolor . " rowdiv'>\n<div  class='admincelldiv'><a href='del_field.php?table=" . $tablename . "&field=" . $row['Field'] . "' onclick='return confirm_delete()'><img src='images/delete.png' border='0' title='Delete'></a></div>\n";
			  echo "<div class='admincelldiv'><a href='admin_column.php?table=" . $tablename . "&field=" . $row['Field'] . "'><img src='images/admin.png' border='0' title='Administer'></a></div>\n"; }
	  
		  else {
	  
			  echo "<div class='rowdiv'><div class='admincelldiv'>X</div>\n";
			  echo "<div class='admincelldiv'>X</div>\n"; }
	  
		  echo "<div class='admincelldiv'>" . $row['Key'] . "</div>\n";
		  echo "<div class='celldiv'>" . $row['Field'] . "</div>\n";
		  echo "<div class='celldiv'>" . $row['Type'] . "</div>\n";
		  echo "<div class='celldiv'>" . urldecode($row['Default']) . "</div>\n";
		  echo "<div class='celldiv'>" . $row['Extra'] . "</div>\n";
		  $xy = explode("|",urldecode($row['Comment']));
		  echo "<div class='celldiv'>" . $xy[0] . "</div>\n";
		  echo "<div class='celldiv'>" . inputTypeText($xy[1]) . "</div>\n";
		  echo "<div class='admincelldiv'>" . YesNo($xy[3]) . "</div></div>\n";
		  }


	mysqli_close($con);


}

//end MySQL management functions


function createFile($filename,$filepath,$str) {

	$myFile = $filepath . $filename;

	$fh = fopen($myFile, 'w') or die("can't open file: " . $myFile);

	fwrite($fh, $str);

	fclose($fh);

	return "Success!";
}

function checkTableExist() {

	global $con;

	$tables = array();

	$sql ='SHOW TABLES FROM ' . CONN_DB;

	$rs = mysqli_query($con,$sql);

   while ($row = mysqli_fetch_row($rs)) $tables[] = $row[0];
    if (!$result) {
    }

   return $tables;

}

function createdbManagerTable() {

$sql = "CREATE TABLE dbmanager (id INT NOT NULL AUTO_INCREMENT, PRIMARY KEY(id), tablename varchar(50), fieldname varchar(50), fieldtype varchar(25),  displaytext varchar(100), size varchar(10), visible tinyint(1), primarysort INT(3), value varchar(300))";
runSQL($sql);

}


function deleteRow($table,$row) {

	if ($row == "all") {
		$sql = "DELETE FROM " . $table; }
	else {
		$sql = "DELETE FROM " . $table . " WHERE `id`=" . $row; }

	runSQL($sql);
}



function addtoTableInfoFile($tableinfo,$curtable) {

if ($tableinfo) {
	$tables = explode("|", $tableinfo);
	if ($curtable) {
		$tables[] = $curtable; }
	$tableinfo = implode("|", $tables); }
else {
	$tables = array();
	$tableinfo = $curtable; }

	$str = "<?php \n";

	$str .= "$" . "tableinfo = \"" . $tableinfo . "\";\n";

	$str .= "?>";

	createFile('tableinfo','includes/',$str);

}

function displayField($fieldtype,$fieldval) {

	if (($fieldtype == FIELD_TYPE_TEXT) || ($fieldtype == FIELD_TYPE_TARE) || ($fieldtype == FIELD_TYPE_SELE)) {
		$returnval = substr(textDecode($fieldval), 0, 30); }
	elseif ($fieldtype == FIELD_TYPE_DATE) {
		$returnval = date('Y-m-d',strtotime($fieldval)); }
	elseif ($fieldtype == FIELD_TYPE_DT) {
		$returnval = date('Y-m-d H:i:s',strtotime($fieldval)); }
	else {
		$returnval = $fieldval; }

	return $returnval;

}

function findAdminTables($tbl_name) {
	$pos = strpos($tbl_name, ADMIN_PREFIX);
	if ($pos === false) {
		return false; }
	else {
		return true; }

}

function inputTypeText($val) {
	if (($val != null) && ($val <= FIELD_TYPE_COUNT)) {
		$input_type = unserialize (FIELD_NAMES);

		return $input_type[$val]; }
	else {
		return "Undefined"; }
}

function YesNo($x) {
	if ($x == "1") {
		return "Yes"; }
	else {
		return "No"; }
}

function addSelectionOptions($options,$tablename,$colname) {

	$oparray = explode("\n",$options);

	$insert_str = "";

	foreach($oparray as $str) {
		$tmp = explode("|",$str);

		if ($insert_str) {
			$insert_str .= ", "; }

		if (($tmp[0] != null) && ($tmp[1])) {
			$insert_str .= "('" . $tablename . "','" . $colname . "'," . $tmp[0] . ",'" . urlencode(trim($tmp[1])) . "')"; }
	}

	$sql = "INSERT INTO " . ADMIN_TABLES_OPTIONS  ." (tbl_name,tbl_column,option_val,option_display) VALUES " . $insert_str;

	return $sql;

}

function displayOptions($val) {

	if ($val == "1") {
		$namelist = unserialize(COL_NAMES);	}
	elseif ($val == "2") {
		$namelist = unserialize(FIELD_NAMES);	}


	foreach($namelist AS $key => $value) {
		buildOption($key,$value);
	}


}

function buildOption($val,$txt) {
	echo "<option value='" . $val . "'>" . $txt . "\n";

}

function isChecked($val) {

	if ($val) {
		return "CHECKED"; }
}

function hasOptions($val) {
	if ($val == "2" || $val == "3" || $val == "4" || $val == "9") {
		return true; }
	else {
		return false; }

}

function getSelectOptions($tablename,$field,$distype,$build) {

	$str = "";
	$buildval = $tablename . "_" . $field;
	$defaultval = $build[$buildval];


$con = mysql_connect(CONN_SERVER,CONN_USER,CONN_PWD);

	mysql_select_db(CONN_DB);

	$sql = "SELECT * FROM " . ADMIN_TABLES_OPTIONS . " WHERE tbl_name='" . $tablename . "' AND tbl_column='" . $field . "' ORDER BY option_val ASC";

	$rs = mysql_query($sql);
    while ($row = mysql_fetch_assoc($rs)) {
    	$id = $row['id'];

    	if ($distype == "1") {
			$str .= "<script>optionsListArray.push(" . $id . ");</script><div id='row" . $id . "'><a href='javascript:delOption(" . $id . ");' onclick='return confirm_delete()'>X</a>&nbsp;&nbsp;&nbsp;<a href='javascript:editOption(" . $id . ");'>E</a>&nbsp;&nbsp;&nbsp;<span id='val" . $id . "'>" . $row['option_val'] . "</span> - <span id='disp" . $id . "'>" . textDecode($row['option_display']) . "</span></div>\n";
		}
		elseif ($distype == "2") {
			if ($defaultval == $row['option_val']) {
				$sel = " SELECTED"; }
			else {
				$sel = ""; }
			$str .= "<option value='" . $row['option_val'] . "'" . $sel . ">" . textDecode($row['option_display']) . "\n";
		}
	}

	mysqli_close($con);


	return $str;
}

function displaySelectOptForEdit($row) {

}

function deleteOption($id) {

	deleteRow(ADMIN_TABLES_OPTIONS,$id);

	return $id;
}

function addUpdateOption($str2) {

			$str = json_decode_nice(stripslashes($str2));

		if (!$str[0]) {
			$options = $str[3] . "|" . $str[4];

				$sql = "INSERT INTO " . ADMIN_TABLES_OPTIONS  ." (tbl_name,tbl_column,option_val,option_display) VALUES ('" . $str[1] . "','" . $str[2] . "','" . $str[3] . "','" . $str[4] . "')";
				$con = mysql_connect(CONN_SERVER,CONN_USER,CONN_PWD);
				mysql_select_db(CONN_DB, $con);

				mysql_query($sql);

				$str[0] = mysql_insert_id();

				mysqli_close($con);

		}
		else {
			$sql = "UPDATE " . ADMIN_TABLES_OPTIONS . " SET option_val=" . $str[3] . ", option_display='" . urlencode(trim($str[4])) . "' WHERE id=" . $str[0];
			runSQL($sql);
		}

	return $str[0];


}

function json_decode_nice($json, $assoc = FALSE) {
    $json = str_replace(array("\n","\r"),"",$json);
    $json = preg_replace('/([{,])(\s*)([^"]+?)\s*:/','$1"$3":',$json);
    return json_decode($json,$assoc);
}

function isUnset($val) {
	if (!$val) {
		return "0"; }
	else {
		return $val; }
}

function setOptionsSession($table,$col) {

	$str = "";
	    $sid = $table . "_" . $col;

	$sql = "SELECT * FROM " . ADMIN_TABLES_OPTIONS . " WHERE tbl_name='" . $table . "' AND tbl_column='" . $col . "' ORDER BY option_val ASC";

	global $con;

	if (!$con) {
	  die('Could not connect: ' . mysqli_error()); }

	$rs = mysqli_query($con,$sql);

    while ($row = mysqli_fetch_assoc($rs)) {
    	$id = $row['option_val'];

    	$_SESSION[$sid][$id] = $row['option_display'];
	}

	mysqli_close($con);

}

function getFieldInfo($tablename) {

	$fields = array();
	$x = 0;

	$sql = "SHOW FULL COLUMNS FROM " . $tablename;
	
	global $con;

	if (!$con) {
	  die('Could not connect: ' . mysqli_error()); }

	  $rs = mysqli_query($con,$sql);

	while ($row = mysqli_fetch_assoc($rs)) {
			$fields[$x][0] = $row['Field'];
			$comments = explode("|",urldecode($row['Comment']));
			$fields[$x][1] = $comments[0];
			$fields[$x][2] = $comments[1];
			$fields[$x][3] = $comments[2];
			$fields[$x][4] = $comments[3];
			$fields[$x][5] = $row['Default'];
			$x++;
	}

	return $fields;

}

function showFormField($tablename,$colname,$fieldtype,$build) {

	$buildkey = $tablename . "_" . $colname;


	if ($colname != "id") {
		switch ($fieldtype) {

			case "0":
				$val = textDecode($build[$buildkey]);
				if (strlen($val) > 20) {
					$val = "<textarea name='" . $colname . "' cols='40' rows='10'  >" . $val . "</textarea>\n";
				}
				else {
					$val = "<input type='text'  name='" . $colname . "' size='20' value='" . $val . "'>\n";
				}
				break;
			case "1":
				$val = "<textarea name='" . $colname . "' cols='40' rows='10'  >" . textDecode($build[$buildkey]) . "</textarea>\n";
				break;
			case "2":
				$val = "<select name='" . $colname . "' >\n";
				$val .= getSelectOptions($tablename,$colname,"2",$build);
				$val .= "</select>\n";
				break;
			case "3":
			case "4":
				$val = "<input type='hidden' name='" . $colname . "' value='0'><input type='checkbox' name='" . $colname . "' value=" . buildCheck($build[$buildkey]) . ">";
				break;
			case "5":
			case "6":
			case "7":
			case "8":
				$val = "<input type='text'  name='" . $colname . "' size='5' value='" . textDecode($build[$buildkey]) . "'>\n";
				break;
			case "9":
				$val = "<select name='" . $colname . "[]' MULTIPLE>\n";
				$val .= getSelectOptions($tablename,$colname,"2",$build);
				$val .= "</select>\n";
				break;
			default:
				$val = "ERROR - Unknown!";
				break;
		}
	}
	else {
		$val = "ID"; }

	return $val;


}

function buildCheck($val) {
	$valcheck = explode("|",$val);
		if ($valcheck[0] == $valcheck[1]) {
		$finalval = "'" . $valcheck[0] . "' CHECKED"; }
	else {
		$finalval = "'" . $valcheck[0] . "'"; }

	return $finalval;
}

function isBool($val) {
	if ($val == "3" || $val == "4") {
		return true; }
	else {
		return false; }

}

function rowColor($val) {
	if ($val == "rowon") {
		return "rowoff"; }
	else {
		return "rowon"; }
}

function delRow($tablename,$id) {

	deleteRow($tablename,$id);

	return $id;
}

function delProc($proc) {
	$sql = "DROP PROCEDURE " . $proc;
	runSQL($sql);

	return $proc;
}

function delFunc($proc) {
	$sql = "DROP FUNCTION " . $proc;
	runSQL($sql);

	return $proc;
}

function saveSort($tablename,$val) {

	if (!$val) {
		$val = "id^d"; }

	if ($tablename) {
		$sql = "DELETE FROM " . ADMIN_TABLES_SORTS . " WHERE tbl_name='" . $tablename . "'";
			runSQL($sql);

		$sql = "INSERT INTO " . ADMIN_TABLES_SORTS . " (tbl_name,sort_default) VALUES ('" . $tablename . "','" . $val . "')";
			runSQL($sql);
	}

}

function getDefaultSort($sort,$tablename) {
	if (!$sort) {
		$sql = "SELECT sort_default FROM " . ADMIN_TABLES_SORTS . " WHERE tbl_name='" . $tablename . "'";
		$var = singleDBCall('sort_default',$sql);

	}
	else {
		$var = $sort; }

	if (!$var) {
		$var = "id^d"; }

	return $var;

}

function setSort($sort) {
	$val = "";

	$sortarray = explode("|", $sort);
	foreach($sortarray as $str) {
		if ($val) {
			$val .= ","; }
		$testsort = explode("^", $str);

		if ($testsort[1] == "u") {
			$val .= $testsort[0] . " DESC"; }
		else {
			$val .= $testsort[0] . " ASC"; }

	}

	return $val;
}

function setSortJS($sort) {

	$val = "";
	$x = 0;
	$sortarray = explode("|", $sort);
	foreach($sortarray as $str) {
		$val .= "sortList[" . $x . "]='" . $str . "';\n";
		$x++;
	}
	return $val;
}

function buildClientConst($cname,$cval) {
	return "window." . strtoupper($cname) . " = \"" . $cval . "\";\n";
}


function buildServerConst($cname,$cval) {
	return "define('" . strtoupper($cname) . "', \"" . $cval . "\");\n";
}

function updateConfigStatus($val) {
	$sql = "UPDATE " . CONFIG_TABLE . " SET config_is_current=0 WHERE config_table='" . $val . "'";

	runSQL($sql);
}

function doEncode($val) {
	return sha1($val,FALSE);
}

function encodeImg($fn,$imgid,$imgtype) {
	$val = $imgid . $imgtype . $fn;
	$retlen = rand(8,12);
	return substr(doEncode($val), 0, $retlen);
}

function getImgType($val) {
	$sql = "SELECT option_display FROM " . ADMIN_TABLES_OPTIONS . " WHERE tbl_name='tbl_name' AND tbl_column='img_type' AND option_val='" . $val ."'";
	return singleDBCall("option_display",$sql);
}

function isSelected($val1,$val2) {
	if ($val1 == $val2) {
		return " SELECTED"; }
}

function textDecode($val) {
	return urldecode(stripslashes($val));
}


?>
<?php
include("includes/header.php");

$sql = "CREATE TABLE IF NOT EXISTS " . ADMIN_TABLES_OPTIONS . " (id INT NOT NULL AUTO_INCREMENT, PRIMARY KEY(id), tbl_name varchar(100), tbl_column varchar(100), option_val INT NOT NULL, option_display varchar(100))";

runSQL($sql);

$sql = "CREATE TABLE IF NOT EXISTS " . ADMIN_TABLES_SORTS . " (id INT NOT NULL AUTO_INCREMENT, PRIMARY KEY(id), tbl_name varchar(100), sort_default varchar(500))";

runSQL($sql);

echo "Setup completed. <a href='index.php'>Click here</a> to begin administering the system.";


?>
